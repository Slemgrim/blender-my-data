import os
from decouple import config, Csv
from unipath import Path
from dj_database_url import parse as db_url

BASE_DIR = Path(__file__).parent.parent
SECRET_KEY = config('SECRET_KEY')
DEBUG = config('DEBUG', default=False, cast=bool)

ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv(), default=["mydata.local", ])

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'rest_framework',
    'web.apps.WebConfig',
    'social_django',
    'webpack_loader',

]

SITE_ID = 1


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

ROOT_URLCONF = 'mydata.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['./templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

WSGI_APPLICATION = 'mydata.wsgi.application'


DATABASES = {
    'default': config(
        'DATABASE_URL',
        default='sqlite:///' + BASE_DIR.child('db.sqlite3'),
        cast=db_url
    )
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
    'web.oauth.blender.BlenderIdOauth2',
    'web.oauth.blender.BlenderIdToken',
    'django.contrib.auth.backends.ModelBackend',
)

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/assets/'
STATIC_ROOT = 'assets'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

BLENDER_ID = {
    "BASE_URL": config('BLENDER_ID_BASE', default='http://id.local:8001/'),
}

BLENDER_OPENDATA = {
    "BASE_URL": config('BLENDER_OPENDATA_BASE', default='http://opendata.local:8002/'),
    "TOKEN": config('BLENDER_OPENDATA_TOKEN')
}

LOGIN_URL = 'auth/login'
LOGOUT_URL = 'auth/logout'
LOGIN_REDIRECT_URL = 'index'
AUTH_USER_MODEL = 'web.MydataUser'

# Social Auth
KEY = config('BLENDER_ID_KEY')
SECRET = config('BLENDER_ID_SECRET')
SCOPE = ['email']
USER_FIELDS = ['username', 'email', 'blender_id']
USE_X_FORWARDED_HOST = True
SOCIAL_AUTH_POSTGRES_JSONFIELD = True

# Webpack
WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG,
        'BUNDLE_DIR_NAME': 'webpack_bundles/', # must end with slash
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': ['.+\.hot-update.js', '.+\.map']
    }
}

WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG
    }
}

