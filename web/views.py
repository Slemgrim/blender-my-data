import urllib

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseServerError, HttpResponseForbidden
from django.shortcuts import render_to_response, redirect
from web.exceptions import ServiceUnavailable
from web.models import Benchmark
from web.blender_opendata import opendata_delete_benchmark


@login_required
def index(request):

    items = Benchmark.get_all_by_blender_id(request.user.blender_id)

    return render_to_response('index.html', {
        'user': request.user,
        'items': items,
        'show_url': urllib.parse.urljoin(settings.BLENDER_OPENDATA['BASE_URL'], 'api/benchmarks'),
    })

@login_required
def benchmark_delete(request, id):
    benchmark = Benchmark.objects.get(pk=id)

    if benchmark is None:
        return redirect('index')

    if not benchmark.belongs_to_blender_id(request.user.blender_id):
        raise HttpResponseForbidden()

    try:
        opendata_delete_benchmark(benchmark.manage_id)
        benchmark.delete()

    except ServiceUnavailable:
        return HttpResponseServerError('Service Unavailable, please try again later')

    return redirect('index')


def logout_view(request):
    logout(request)
    return redirect('index')
