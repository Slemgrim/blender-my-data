from rest_framework.exceptions import APIException, _get_error_details


class ServiceUnavailable(APIException):
    status_code = 503
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 'service_unavailable'


class ServiceBadRequest(APIException):
    status_code = 400
    default_detail = 'Bad Request'
    default_code = 'bad_request'

    def __init__(self, detail=None, code=None):
        if detail is None:
            detail = self.default_detail
        if code is None:
            code = self.default_code

        self.detail = _get_error_details(detail, code)
