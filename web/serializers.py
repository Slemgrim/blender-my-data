from rest_framework import serializers

from web.models import Benchmark, Token


class BenchmarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Benchmark
        fields = ('blender_id', 'benchmark_id', 'manage_id')


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('hash', 'user')