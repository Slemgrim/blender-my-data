import os
import requests

from urllib.parse import urljoin
from django.conf import settings
from rest_framework import exceptions as ex
from web import exceptions
from web.exceptions import ServiceBadRequest


def blender_opendata_base_url():
    """Gets URL to Blender Opendata.

    URL can be changed in settings or through BLENDER_OPENDATA_ENDPOINT env variable

    @returns: string url to Blender Opendata
    """
    return os.environ.get('BLENDER_OPENDATA_ENDPOINT', settings.BLENDER_OPENDATA['BASE_URL'])


def opendata_submit_benchmark(benchmark, blender_id):
    """submits benchmark to opendata service

    @param benchmark: dict benchmark
    @param blender_id: the users unique Blender ID

    @returns: dict response.
       return the benchmarks id and the manage id which can be used to delete the benchmark
       manage_id should be saved somewhere since this is the only place where it is visible
    """

    headers = {"Authorization": "Token " + settings.BLENDER_OPENDATA['TOKEN']}
    try:
        url = urljoin(settings.BLENDER_OPENDATA['BASE_URL'], 'api/benchmarks/')
        r = requests.post(url, json=benchmark, verify=True, headers=headers)

    except requests.exceptions.ConnectionError:
        raise exceptions.ServiceUnavailable

    if r.status_code == 400:
        raise ServiceBadRequest(r.json())

    if r.status_code != 201:
        raise ex.ParseError

    response = r.json()

    data = dict()
    data['manage_id'] = response['manage_id']
    data['blender_id'] = blender_id
    data['benchmark_id'] = response['benchmark_id']

    return data


def opendata_delete_benchmark(id):
    """delete benchmark at opendata service

    @param id: id of benchmark

    @raises: ServiceUnavailable exception when opendata isn't available
    @raises: ParseError when something went wrong during deletion

    """
    try:
        url = urljoin(settings.BLENDER_OPENDATA['BASE_URL'], urljoin('api/benchmarks/', id))
        r = requests.delete(url)

    except requests.exceptions.ConnectionError:
        raise exceptions.ServiceUnavailable

    if (r.status_code != 204) and (r.status_code != 404):
        raise ex.ParseError
