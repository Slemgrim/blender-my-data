import re
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication
from web.models import Token


class TokenAuth(BaseAuthentication):
    def authenticate(self, request):

        auth_hash = request.META.get('HTTP_AUTHORIZATION')
        if auth_hash is None:
            return None

        try:
            auth_hash = re.search('Bearer (.*)', auth_hash).group(1)
        except AttributeError:
            return None

        try:
            token = Token.objects.get(hash=auth_hash)
        except (Token.DoesNotExist):
            raise exceptions.AuthenticationFailed('invalid token')

        return token.user, token