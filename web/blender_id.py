import os
import urllib
import requests
import requests.exceptions
from web import exceptions
from django.conf import settings
from web.models import MydataUser

def blender_id_base_url():
    """Gets URL to Blender ID.

    URL can be changed in settings or through BLENDER_ID_ENDPOINT env variable

    @returns: string url to Blender ID
    """
    return os.environ.get('BLENDER_ID_ENDPOINT', settings.BLENDER_ID['BASE_URL'])


def blender_id_server_validate(token):
    """Validate the auth token with the server.

    @param token: the authentication token
    @type token: str
    @returns: tuple (expiry, error).
        The expiry is the expiry date of the token if it is valid, else None.
        The error is None if the token is valid, or an error message when it's invalid.
    """

    try:
        url = urllib.parse.urljoin(blender_id_base_url(), 'api/me')
        r = requests.get(url, headers={'Authorization': 'Bearer ' + token}, verify=True)

    except requests.exceptions.ConnectionError:
        raise exceptions.ServiceUnavailable

    if r.status_code != 200:
        raise MydataUser.DoesNotExist

    response = r.json()
    return response['id']
