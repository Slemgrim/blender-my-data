import os
import re
import requests
import requests.exceptions
from web import exceptions
from django.conf import settings
from web.models import MydataUser
from rest_framework import exceptions
from social_core.backends.oauth import BaseOAuth2
from rest_framework.authentication import BaseAuthentication


class BlenderIdOauth2(BaseOAuth2):
    """Blender ID oAuth 2 Provider"""

    name = 'blender-id-oauth2'
    BASE_URL = os.environ.get('BLENDER_ID_ENDPOINT', settings.BLENDER_ID['BASE_URL'])
    API_URL = BASE_URL + 'api/'
    AUTHORIZATION_URL = BASE_URL + 'oauth/authorize'
    ACCESS_TOKEN_URL = BASE_URL + 'oauth/token'
    ACCESS_TOKEN_METHOD = 'POST'
    SCOPE_SEPARATOR = ','
    EXTRA_DATA = [
        ('expires', 'expires'),
        ('access_token', 'access_token'),
        ('refresh_token', 'refresh_token')
    ]

    def get_user_details(self, response):
        return {'blender_id': response.get('id'),
                'username': response.get('nickname'),
                'email': response.get('email') or '',
                'first_name': response.get('full_name')}

    def user_data(self, access_token, *args, **kwargs):
        """Loads user data from service"""
        url = self.API_URL + 'me'

        headers = {
            "Authorization": 'Bearer ' + access_token
        }

        return self.get_json(url, headers=headers)

    def authorize_token(self, token):
        """Validate the auth token with the server.

        @param token: the access_token
        @type token: str
        @returns: tuple (expiry, error).
            The expiry is the expiry date of the token if it is valid, else None.
            The error is None if the token is valid, or an error message when it's invalid.
        """

        try:
            url = self.API_URL + 'me'
            r = requests.get(url, headers={'Authorization': 'Bearer ' + token}, verify=True)

        except requests.exceptions.ConnectionError:
            raise exceptions.ServiceUnavailable

        if r.status_code != 200:
            raise MydataUser.DoesNotExist

        response = r.json()
        return response['id']


class BlenderIdToken(BaseAuthentication):
    """Authentication backend for Blender Id access_token

    Requests must have a Authorization: Bearer header containing a valid Blender ID access_token
    """
    def authenticate(self, request):

        token = request.META.get('HTTP_AUTHORIZATION')
        if token is None:
            return None

        try:
            token = re.search('Bearer (.*)', token).group(1)
        except AttributeError:
            return None

        try:
            blender_id = BlenderIdOauth2().authorize_token(token)
            user = MydataUser.get_by_blender_id(blender_id)
        except MydataUser.DoesNotExist:
            raise exceptions.AuthenticationFailed('invalid token')

        return user, token