from django.conf.urls import url
from django.contrib.auth.views import LoginView
from django.urls import path, include

from web import views, api_views

urlpatterns = [
    url('', include('social_django.urls', namespace='social')),
    url(r'^$', views.index, name='index'),
    path('benchmarks/delete/<int:id>', views.benchmark_delete, name='delete'),

    path('auth/login/', LoginView.as_view(), name='login'),
    path('auth/logout/', views.logout_view, name='logout'),

    url(r'^api/benchmarks/$', api_views.BenchmarkSubmit.as_view()),
    url(r'^api/token/$', api_views.token_view),
]
