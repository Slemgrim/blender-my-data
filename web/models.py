import uuid

from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser


class Benchmark(models.Model):
    blender_id = models.CharField(max_length=100, blank=False)
    benchmark_id = models.CharField(max_length=100, blank=False, )
    manage_id = models.CharField(max_length=100, blank=False)
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('date_created',)

    def save(self, *args, **kwargs):
        super(Benchmark, self).save(*args, **kwargs)

    @staticmethod
    def get_all_by_blender_id(blender_id):
        return Benchmark.objects.filter(blender_id=blender_id)

    def belongs_to_blender_id(self, blender_id):
        return self.blender_id == blender_id


class MydataUser(AbstractUser):
    """Custom user with added blender id property"""
    blender_id = models.CharField(max_length=100)

    @staticmethod
    def get_by_blender_id(blender_id):
        return MydataUser.objects.get(blender_id=blender_id)


class Token(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    hash = models.CharField(max_length=200, db_index=True)
    date_created = models.DateTimeField(auto_now_add=True)
    is_valid = models.BooleanField(default=True)

    class Meta:
        ordering = ('date_created',)