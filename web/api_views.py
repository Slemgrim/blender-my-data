import uuid

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotAuthenticated
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from web.blender_opendata import opendata_submit_benchmark
from web.serializers import BenchmarkSerializer, TokenSerializer
from web.token_auth import TokenAuth


class BenchmarkSubmit(APIView):
    """
    API endpoint that allows users to submit benchmarks
    """
    authentication_classes = (TokenAuth,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """Validates token with Blender ID and sends benchmark to Blender Opendata"""
        benchmark_data = opendata_submit_benchmark(request.data, request.user.blender_id)

        serializer = BenchmarkSerializer(data=benchmark_data)
        if serializer.is_valid():
            serializer.save()
            return Response(dict(
                benchmark_id=serializer.data['benchmark_id'],
                manage_id=serializer.data['manage_id']
            ), status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def token_view(request):
    if not request.user.is_authenticated:
        raise NotAuthenticated(detail="Not logged in. Use your web browser to login first", code=None)

    if request.method == 'GET':

        data = {
            "user": request.user.id,
            "hash": uuid.uuid4().hex
        }

        serializer = TokenSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            data = serializer.data
            del(data['user'])
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)