# Blender Mydata

Public open data portal for benchmark results

## Requirements

- The example config expects a running version of [Blender Opendata](https://gitlab.com/Slemgrim/blender-open-data) under 
http://mydata.local for development. We don't have a production version yet.

- You also need a APP-ID and APP-SECRET from [Blender ID](https://blender.org/id) 
or your local version of Blender ID (http://id.local)

## Development setup

After cloning the Git repo, perform these steps to create a working dev server:

1. copy `.env-sample` to `.env` and adjust to your needs
2. Start your virtaulenv with `pipenv shell` 
3. Install requirements with `pipenv install`
4. Setup database `docker-compose up` or provide a database in `.env`
5. Run migrations  `./manage.py migrate`
6. Install frontend dependencies `npm install`
7. Serve frontend  `npm run serve`
8. Run application `./manage.py runserver`


## Try it out

1. Submit an example benchmark with a valid Blender ID access_token
`$ sh examples/example.sh examples/benchmark.json your-blender-id-token`
You have to create the access_token manually at the moment since we 
don't have an authorized benchmark tool yet

2. Open running application in Browser, login with Blender ID and manage your benchmarks.
