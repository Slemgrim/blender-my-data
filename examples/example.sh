# Get an auth token fron http://mydata.local/api/token and use it to submit benchmarks.
# You only can get a token if you have a valid login session in your browser
# run it as follows `example.sh path-to-benchmark your-token`

curl_opts=(
    -H "Content-Type: application/json"
    -H "Accept: application/json"
    -H "Authorization: Bearer $2"
    -X POST
    --data-binary "@$1"
)
curl "${curl_opts[@]}" http://localhost:8000/api/benchmarks/